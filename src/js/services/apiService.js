angular.module("cineacesso")
.factory('serviceAPI', ['$http', '$httpParamSerializerJQLike', '$uibModalStack', serviceAPI]);
function serviceAPI($http, $httpParamSerializerJQLike, $uibModalStack) {

  const config = { apiURL: 'http://127.0.0.1:5053'};

  var _getStatus = function (callback) {
    callback($http.get(config.apiURL+'/api/status'));
  };

  var _preloadVideo = function (videoId, callback) {
    callback($http.get(config.apiURL+'/api/preload/'+videoId));
  };

  var _stopVideo = function (callback) {
    callback($http.get(config.apiURL+'/api/stop'));
  };

  var _startVideo = function (callback) {
    callback($http.get(config.apiURL+'/api/start'));
  };

  var _delVideo = function (videoId, callback) {
    callback($http.delete(config.apiURL+'/api/users/'+videoId));
  };

  var _delTicket = function (ticketId, callback) {
    callback($http.delete(config.apiURL+'/api/ticket/'+ticketId));
  };

  var _readAll = function(callback) {
    callback($http.get(config.apiURL+'/api/videos/list'));
  };

  var _cancelPreload = function(callback) {
    callback($http.get(config.apiURL+'/api/preload/cancel'));
  };

  var _resetUsers = function(callback) {
    callback($http.get(config.apiURL+'/api/users/reset'));
  };

  var _getClientList = function(callback) {
    callback($http.get(config.apiURL+'/api/users/list'));
  };

  var _getTicketList = function(callback) {
    callback($http.get(config.apiURL+'/api/ticket'));
  };

  var _getSyncTime = function(callback) {
    callback($http.get(config.apiURL+'/api/rsync'));
  };

  var _setSyncTime = function(sync, callback) {
    callback($http.post(config.apiURL+'/api/rsync', 'rsynctime='+sync.value, {
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }));
  };

  var _addTicket = function(ticket, callback) {
    callback($http.post(config.apiURL+'/api/ticket', 'ticket='+ticket.cod, {
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }));
  };

  var _setTime = function(newTime, callback) {
    callback($http.post(config.apiURL+'/api/time', 'time='+newTime, {
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }));
  };

  var _sendRequest = function (video, files, callback) {
    var fd = new FormData();
    var forCount = 0;

    var url = "/api/videos/add";

    fd.append("title", video.title);
    fd.append("author", video.author);
    fd.append("year", video.year);
    fd.append("type", video.type);

    for (var i = 0; i < files.length; i++) {
        fd.append("files", files[forCount]);
        forCount++;
    }

    callback($http.post(config.apiURL+url, fd, {
      withCredentials : false,
      headers : {
        'Content-Type' : undefined
      },
      transformRequest : angular.identity
    }));
  };

  return {
    sendRequest: _sendRequest,
    readAll: _readAll,
    preloadVideo: _preloadVideo,
    stopVideo: _stopVideo,
    startVideo: _startVideo,
    delVideo: _delVideo,
    delTicket: _delTicket,
    getStatus: _getStatus,
    resetUsers: _resetUsers,
    getClientList: _getClientList,
    getSyncTime: _getSyncTime,
    setSyncTime: _setSyncTime,
    getTicketList: _getTicketList,
    addTicket: _addTicket,
    setTime: _setTime,
    configAPI: config,
    cancelPreload: _cancelPreload
  };
};
