angular.module('cineacesso')

.controller('ModalSyncCtrl', ['$http', '$scope', 'serviceAPI', '$cookieStore', '$uibModalStack', '$rootScope', 'toastr', ModalSyncCtrl]);

function ModalSyncCtrl($http, $scope, serviceAPI, $cookieStore, $uibModalStack, $rootScope, toastr) {

  $scope.sync = {};

  $scope.cancel = function () {
    $uibModalStack.dismissAll();
  };

  $scope.changeSync = function() {
    serviceAPI.setSyncTime($scope.sync, function (requestStatus) {
      requestStatus.success(function (data) {
        $uibModalStack.dismissAll();
        $rootScope.$broadcast('changeSync', 'atualize');
        toastr.success("O tempo de sync foi atualizado.");
      }),
      requestStatus.error(function (data) {
        $uibModalStack.dismissAll();
        toastr.error("Não foi possível atualizar o tempo de sync.");
      });
    });
  };

  var getSyncTime = function () {
    serviceAPI.getSyncTime(function (data) {
      data.success(function (data) {
        $scope.sync = data;
      });
    });
  };

  getSyncTime();

};
