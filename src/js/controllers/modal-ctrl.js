angular.module('cineacesso')

.controller('ModalCtrl', ['$http', '$scope', 'serviceAPI', '$cookieStore', '$uibModalStack', '$rootScope', 'toastr', ModalCtrl]);

function ModalCtrl($http, $scope, serviceAPI, $cookieStore, $uibModalStack, $rootScope, toastr) {

  $scope.files = [];

  $scope.cancel = function () {
    $uibModalStack.dismissAll();
  };

  $scope.uploadFiles = function(file) {
    $scope.$apply(function () {
      $scope.files.push(file.files[0]);
    });
  };

  $scope.newVideo = function(video) {
    console.log($scope.files);
    serviceAPI.sendRequest(video, $scope.files, function (requestStatus) {
      requestStatus.success(function (data) {
        $uibModalStack.dismissAll();
        $rootScope.$broadcast('newVideo', 'atualize');
        toastr.success("O vídeo foi adicionado com sucesso!");
      }),
      requestStatus.error(function (data) {
        $uibModalStack.dismissAll();
        toastr.error("Não foi possível adicionar o video.");
      });
    });
  };

};
