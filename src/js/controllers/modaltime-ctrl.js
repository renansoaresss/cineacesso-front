angular.module('cineacesso')

.controller('ModalTimeCtrl', ['$http', '$scope', 'serviceAPI', '$cookieStore', '$uibModalStack', '$rootScope', 'toastr', ModalTimeCtrl]);

function ModalTimeCtrl($http, $scope, serviceAPI, $cookieStore, $uibModalStack, $rootScope, toastr) {

  $scope.time = {};

  $scope.cancel = function () {
    $uibModalStack.dismissAll();
  };

  $scope.changeTime = function(time) {
    serviceAPI.setTime($scope.time.value, function (timeStatus) {
      timeStatus.success(function (data) {
        toastr.success("O tempo foi alterado com sucesso.");
        var video = angular.element(document.querySelector('#videoPlayer'));
        var currentTime = Math.round(video["0"].currentTime);
        video["0"].currentTime = currentTime + (Number($scope.time.value)/1000);
        $uibModalStack.dismissAll();
      })
      timeStatus.error(function (data) {
        toastr.error("Não foi possível mudar o tempo.");
        $uibModalStack.dismissAll();
      });
    });
  };

};
