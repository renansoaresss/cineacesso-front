/**
 * Master Controller
 */

angular.module('cineacesso')
    .controller('MasterCtrl', ['$http', '$scope', '$cookieStore', '$uibModal', MasterCtrl]);

function MasterCtrl($http, $scope, $cookieStore, $uibModal) {

    var mobileView = 992;

    $scope.animationsEnabled = true;

    $scope.openModal = function (size) {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'templates/_modalVideo.html',
        controller: 'ModalCtrl',
        size: size
      });
    };

    $scope.openModalSync = function (size) {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'templates/_modalSync.html',
        controller: 'ModalSyncCtrl',
        size: size
      });
    };

    $scope.openModalTime = function (size) {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'templates/_modalTime.html',
        controller: 'ModalTimeCtrl',
        size: size
      });
    };

    $scope.openModalTicket = function (size) {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'templates/_modalTicket.html',
        controller: 'ModalTicketCtrl',
        size: size
      });
    };

    $scope.getWidth = function() {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function(newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = ! $cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function() {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function() {
        $scope.$apply();
    };
}
