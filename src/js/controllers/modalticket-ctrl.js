angular.module('cineacesso')

.controller('ModalTicketCtrl', ['$http', '$scope', 'serviceAPI', '$cookieStore', '$uibModalStack', '$rootScope', 'toastr', ModalTicketCtrl]);

function ModalTicketCtrl($http, $scope, serviceAPI, $cookieStore, $uibModalStack, $rootScope, toastr) {

  $scope.ticket = {};
  $scope.ticketList = [];

  $scope.cancel = function () {
    $uibModalStack.dismissAll();
  };

  $scope.addTicket = function() {
    serviceAPI.addTicket($scope.ticket, function (apiRequest) {
      apiRequest.success(function () {
        toastr.success("Ingresso adicionado com sucesso.");
        getTicketList();
      })
      apiRequest.error(function () {
        toastr.error("Não foi possivel adicionar o ingresso.");
      });
    });
  }

  $scope.delTicket = function(ticketId) {
    serviceAPI.delTicket(ticketId, function (apiRequest) {
      apiRequest.success(function (data) {
        getTicketList();
        toastr.success("Ingresso deletado com sucesso.");
      })
      apiRequest.error(function (data) {
        toastr.error("Houve um erro ao deletar o ingresso.");
      });
    });
  };

  var getTicketList = function () {
    serviceAPI.getTicketList(function (apiRequest) {
      apiRequest.success(function (serverTicketList) {
        $scope.ticketList = serverTicketList;
      });
    });
  };

  getTicketList();

}
