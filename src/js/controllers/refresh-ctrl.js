angular.module("cineacesso")
.controller("RefreshCtrl", ['$scope', '$rootScope', '$interval', 'serviceAPI', 'toastr', RefreshCtrl])

function RefreshCtrl($scope, $rootScope, $interval, serviceAPI, toastr) {

  $scope.clientList = [];

  var refreshClients = function() {
    serviceAPI.getClientList(function (requestStatus) {
      requestStatus.success(function (data) {
        $scope.clientList = data;
        $rootScope.$broadcast('newClient', data.length);
      })
      requestStatus.error(function (data) {
        toastr.error("Não foi possível atualizar a lista de clientes. Verifique a conexão com o servidor.", "Erro");
      })
    });
  };
  $interval(refreshClients, 5000);
}
