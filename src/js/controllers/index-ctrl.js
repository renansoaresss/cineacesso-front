angular.module('cineacesso')
.controller('IndexCtrl', ['$http', '$scope', '$cookieStore', '$rootScope', '$sce', 'serviceAPI', 'toastr', IndexCtrl]);

function IndexCtrl($http, $scope, $cookieStore, $rootScope, $sce, serviceAPI, toastr) {

  $scope.videoList = [];
  $scope.qtdClients = 0;
  $scope.actualVideo = {};
  $scope.sync = [];

  $scope.$on('newVideo', function(event, mass) { listVideos(); });

  $scope.$on('changeSync', function(event, mass) { getSyncTime(); });

  $scope.$on('newClient', function(event, mass) { $scope.qtdClients = mass; });

  $scope.preloadVideo = function(videoId) {
    serviceAPI.preloadVideo(videoId, function (preloadStatus) {
      preloadStatus.success(function (data) {
        // $scope.actualVideo.started = "false";
        toastr.success("Video adicionado com sucesso em pré-load.");
        $scope.videoPreload = true;
        getStatus();
      })
      preloadStatus.error(function (data) {
        toastr.error("Não foi possível colocar o vídeo em pré-load.");
        getStatus();
      });
    });
  };

  $scope.cancelPreload = function () {
    serviceAPI.cancelPreload(function (preloadStatus) {
      preloadStatus.success(function (data) {
        toastr.success("Pré-load cancelado com sucesso.");
        getStatus();
      })
      preloadStatus.error(function (data) {
        toastr.error("Houve um erro ao cancelar o pré-load.");
        getStauts();
      });
    });
  }

  $scope.stopVideo = function() {
    serviceAPI.stopVideo(function (stopStatus) {
      stopStatus.success(function (data) {
        toastr.success("Vídeo parado com sucesso.");
        var video = angular.element(document.querySelector('#videoPlayer'));
        video["0"].pause();
        video["0"].currentTime = 0;
        getStatus();
      })
      stopStatus.error(function (data) {
        getStatus();
        toastr.error("Não possível parar o vídeo.", "Erro");
      });
    });
  };

  $scope.startVideo = function() {
    serviceAPI.startVideo(function (startStatus) {
      startStatus.success(function (data) {
        toastr.success("Vídeo iniciado com sucesso.");
        var video = angular.element(document.querySelector('#videoPlayer'));
        video["0"].currentTime = 0;
        video["0"].play();
        // video["0"].onseeked = function () {
        //   if (this.paused) {
        //     var endTime = video["0"].currentTime;
        //     serviceAPI.setTime(endTime, function (timeStatus) {
        //       timeStatus.success(function (data) {
        //         toastr.success("O tempo foi alterado com sucesso.");
        //       })
        //       timeStatus.error(function (data) {
        //         toastr.error("Não foi possível mudar o tempo.");
        //       });
        //     })
        //   }
        // }
        // video["0"].onclick = function() {
        //   var startTime = video["0"].currentTime;
        //   console.log("Start time " + startTime);
        // }
        getStatus();
      })
      startStatus.error(function (data) {
        getStatus();
        toastr.error("Não possível parar o vídeo.");
      });
    });
  };

  $scope.resetUsers = function() {
    serviceAPI.resetUsers(function (resetStatus) {
      resetStatus.success(function (data) {
        toastr.success("A lista de usuários foi resetada com sucesso.");
      })
      resetStatus.error(function (data) {
        toastr.error("Houve um erro para limpar a lista de usuários.");
      });
    });
  };

  $scope.delVideo = function(videoId) {
    serviceAPI.delVideo(videoId, function (delStatus) {
      delStatus.success(function (data) {
        listVideos();
        toastr.success("Video deletado com sucesso.");
      })
      delStatus.error(function (data) {
        toastr.error("Houve um erro ao deletar o video. Verifique se há um video em execução.");
      });
    });
  };

  $scope.getThumbnailURL = function(videoId) {
    return ($sce.trustAsResourceUrl(serviceAPI.configAPI.apiURL+'/img/'+videoId+'.png'));
  };

  $scope.getVideoURL = function(videoUrl) {
    return ($sce.trustAsResourceUrl(serviceAPI.configAPI.apiURL+'/'+videoUrl));
  };

  var listVideos = function () {
    serviceAPI.readAll(function (data) {
      data.success(function (data) {
        $scope.videoList = data;
      });
    });
  };

  var getStatus = function() {
    serviceAPI.getStatus(function (preloadVideoStatus) {
      preloadVideoStatus.success(function (data) {
        $scope.actualVideo = data;
      })
      preloadVideoStatus.error(function (data) {
        $scope.actualVideo.started = "unknown";
      });
    });
  };

  var getSyncTime = function () {
    serviceAPI.getSyncTime(function (data) {
      data.success(function (data) {
        $scope.sync = data;
      });
    });
  };

  getSyncTime();
  getStatus();
  listVideos();

}
