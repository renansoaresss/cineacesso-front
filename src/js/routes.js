'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('cineacesso')

  .config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'templates/dashboard.html'
            })
            .state('newvideo', {
                controller: 'ModalCtrl',
                url: '/newvideo',
                templateUrl: 'templates/_modalVideo.html'
            })
            .state('tables', {
                url: '/tables',
                templateUrl: 'templates/tables.html'
            });
    }
]);
